These are packages in the Manjaro Strit repository that are not in the Manjaro repositories.

## How to access this repo:
You need to be running Manjaro or Arch Linux to use this repo.

Add it by opening your `/etc/pacman.conf` with

`sudo nano /etc/pacman.conf`

and add the following to it at the buttom:

```
[manjaro-strit]
SigLevel = Optional
Server = https://www.strits.dk/files/manjaro-strit/manjaro-strit-repo/$arch
```
Save the files with `CTRL+O` and exit the editor with `CTRL+X`. Then run `sudo pacman -Syy manjaro-strit-keyring` 
to update the databases and install the keyring needed to install packages from the repository.

## Package list:
* arena-tracker-bin (Card tracker for hearthstone)
* artwork-amd-red (Plasma and GTK themes)
* artwork-ghost (Plasma and GTK themes)
* biblioteq-git (Library application)
* breeze-red-cursor-theme (cursor theme)
* cantata-novlc (Cantata without VLC dependency)
* gpg-encrypt-decrypt (Small encryption scripts)
* griffith (Movie manager)
* hearthstone-wallpapers (Wallpapers)
* hunspell-da (Danish spellcheck)
* libc++ (dependency for discord)
* makemkv (Bluray/DVD ripper)
* manjaro-arm-installer (Scripts to install Manjaro-ARM on SD/eMMc cards)
* manjaro-arm-tools (build scripts for Manjaro ARM)
* manjaro-arm-tools-dev (build scripts for Manjaro ARM - Developement branches)
* manjaro-danish-mirror (Danish manjaro mirror - mirror.strits.dk)
* manjaro-strit-donate (Service to mine Monero as donations)
* manjaro-strit-keyring (**Important!** Needed to install packages from this repository)
* mumble-git (VoIP software)
* nvda-remote-server (NVDA Remote Relay server)
* opentracker (Torrent tracker software)
* pia-manager (VPN service provider)
* python-pyautogui (dependency of arena-tracker)
* python-tmdbsimple (optional dependency of griffith)
* rocketchat-client-bin (Chat application)
* sardi-colora-variations-icons-git (icon theme)
* track-o-bot-git (Card tracker for Hearthstone)
* wallpaper-strit (Wallpapers)
* wire-desktop (Chat application)
